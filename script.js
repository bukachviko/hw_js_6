"use strict"

function createNewUser() {
    const user = {
        firstName: "",
        lastName: "",
        birthday: "",

        set setFirstName(userFirstName) {
            if (userFirstName.length < 1) {
                alert("FirstName is not valid");

                return;
            }

            Object.defineProperty(user, "firstName", {
                writable: false,
                value: userFirstName,
            });
        },

        set setLastName(userLastName) {
            if (userLastName.length < 1) {
                alert("LastName is not valid");

                return;
            }

            Object.defineProperty(user, "lastName", {
                writable: false,
                value: userLastName,
            });
        },

        set setBirthday(userBirthday) {
            if (userBirthday.length !== 10) {
                alert("Birthday is not valid");

                return;
            }

            Object.defineProperty(user, "birthday", {
                writable: false,
                value: userBirthday,
            });
        },

        getLogin() {
            return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
        },

        getBirthdayAsDateObject() {
            let dateTime = this.birthday.split(".");
            if (dateTime.length !== 3) {
                alert("Date format is invalid");

                return;
            }

            let dateBirthday = new Date();
            dateBirthday.setFullYear(dateTime[2], dateTime[1] - 1, dateTime[0]);

            return dateBirthday;
        },

        getAge() {
            let currentDate = new Date();
            let diff = currentDate.getTime() - this.getBirthdayAsDateObject().getTime();
            return Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
        },

        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + (this.getBirthdayAsDateObject().getYear() + 1900);
        }
    };

    let userFirstName = prompt("Enter your first name");
    if (userFirstName === null) {
        return;
    }

    let userLastName = prompt("Enter your last name");
    if (userLastName === null) {
        return;
    }
    let userBirthday = prompt("Enter your date of birth in the format dd.mm.yyyy");
    if (userBirthday === null) {
        return;
    }

    user.setFirstName = userFirstName;
    user.setLastName = userLastName;
    user.setBirthday = userBirthday;

    return user;
}

let newUser = createNewUser();

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

/*Відповіді на теорію:
1.Екранування це пошук спеціальних символів у коді, для використовування потрібно додати слєш до спецсимвола який потрібно знайти.
Використовується у програмування идля того щоб допомогти з екрануванням спеціалтних символів юнікоду в рдкове літеральне значення в лапках для вихідного
коду JS, a також розекранує його.
2.Засоби оголошення функції: присвоєння ім'я функції, список аргументів (параметрів) які фунція прийматиме (), інструкції які будуть виконуватись після визову функції {}
3. hoisting -  це механізм в якому змінні та оголошення функцій пересуваються в гору своєї видимості перед тим, як код буде виконано.
це значе, що не важливо де були оголошені функції або змінні, всі вони пересуваються уверх своєї області відімості, незалежно від того локальна вона чи глобальна
 */

